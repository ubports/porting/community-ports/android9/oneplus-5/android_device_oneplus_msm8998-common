#!/usr/bin/python3

import dbus
import lsb_release
from enum import Enum

class SwitchState(Enum):
    TOP = 1
    MIDDLE = 2
    BOTTOM = 3

with open('/sys/class/switch/tri-state-key/state', 'r') as content_file:
    state = int(content_file.read())

    session = dbus.SystemBus()
    proxy = session.get_object('org.freedesktop.Accounts','/org/freedesktop/Accounts/User32011')
    interface = dbus.Interface(proxy,'org.freedesktop.DBus.Properties')

    try:
        release = lsb_release.get_os_release()["CODENAME"]
    except AttributeError:
        release = "xenial"
    soundIface = "com." + ("ubuntu" if release == "xenial" else "lomiri") + ".touch.AccountsService.Sound"
    silentEnabled = (state == SwitchState.TOP.value)
    interface.Set(soundIface, 'SilentMode', silentEnabled)
